﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_CalvoEric
{
    public class MyVector
    {
        int[] PuntInici;
        int[] PuntFinal;

        public MyVector() { }
        public MyVector(int[] PuntI, int[] PuntF)
        {
            PuntInici = PuntI;
            PuntFinal = PuntF;
        }

        public int[] getPuntInici()
        {
            return PuntInici;
        }

        public int[] getPuntFinal()
        {
            return PuntFinal;
        }

        public void setPuntInici(int[] PuntI)
        {
            PuntInici = PuntI;
        }

        public void setPuntFinal(int[] PuntF)
        {
            PuntFinal = PuntF;
        }

        public double DistanciaVector()
        {
             return Math.Sqrt(Math.Pow(Math.Abs(PuntFinal[0] - PuntInici[0]), 2) + Math.Pow(Math.Abs(PuntFinal[1] - PuntInici[1]), 2));
        }


        public void CanviarDirecció()
        {
            PuntInici = new int[] { PuntInici[0] * -1, PuntInici[1] * -1 };
            PuntFinal = new int[] { PuntFinal[0] * -1, PuntFinal[1] * -1 };
        }

        public override string ToString()
        {
            return "Punt inicial del vector = (" + PuntInici[0] + ", " + PuntInici[1] + ")\nPunt Final del vector = (" + PuntFinal[0] + ", " + PuntFinal[1] + ")\nDistància vector: " + DistanciaVector();
        }


    }
}
