﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_CalvoEric
{
    class VectorGame
    {
        public static MyVector[] randomVectors(int numVectors)
        {
            Random rand = new Random();
            MyVector[] RandomVec = new MyVector[numVectors];
            for (int i = 0; i < numVectors; i++)
            {
                RandomVec[i] = new MyVector();
                RandomVec[i].setPuntInici(new int[] { rand.Next(-10, 10), rand.Next(-10, 10) });
                RandomVec[i].setPuntFinal(new int[] { rand.Next(-10, 10), rand.Next(-10, 10) });
            }
            return RandomVec;
        }

        public static MyVector[] sortVectors(MyVector[] diablito)
        {
            MyVector[] diablitoOrdenado;
            
            for (int i = 1; i < diablito.Length; i++)
            {
                for (int j = diablito.Length - 1; j >= i; j--)
                {
                    if (diablito[j-1].DistanciaVector() > diablito[j].DistanciaVector())
                    {
                        MyVector aux = diablito[j-1];
                        diablito[j-1] = diablito[j];
                        diablito[j] = aux;
                    }
                }
            }
            diablitoOrdenado = diablito;
            return diablitoOrdenado;
        }
    }
}
