﻿using System;

namespace M17_UF1_E1_MyVector_CalvoEric
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Escolleix una de les opcions següents: \n1. Introducció Hello {name}\n2. MyVector\n3. VectorGame\n0. Sortir");
            int opcio = Convert.ToInt32(Console.ReadLine());
            switch (opcio)
            {
                case 1:
                    Nom.Nombre();
                    break;
                case 2:
                    MyVector vector = new MyVector();
                    vector.setPuntInici(new int[] { 3, 4 });
                    vector.setPuntFinal(new int[] { 5, 7 });
                    Console.WriteLine(vector);
                    break;
                case 3:
                    Console.WriteLine("1. randomVector\n2. sortVectors");
                    int opcio2 = Convert.ToInt32(Console.ReadLine());
                    switch (opcio2)
                    {
                        case 1:
                            Console.WriteLine("Insereix el número de vectors aleatoris que vulguis.");
                            int numVectors = Convert.ToInt32(Console.ReadLine());
                            foreach (var mamita in VectorGame.randomVectors(numVectors))
                            {
                                Console.WriteLine(mamita);
                            }
                                break;
                        case 2:
                            
                            MyVector[] DiablitoOrdenado = VectorGame.sortVectors(VectorGame.randomVectors(5));
                            
                            foreach (var diablo in DiablitoOrdenado)
                            {
                                Console.WriteLine(diablo);
                            }
                                break;
                        default:
                            Console.WriteLine("Eres bobo, presiona 1 o 2 inútil.");
                            break;
                    }
                    break;
                case 0:
                    break;
            }
        }
    }
}
